<?php

namespace Zend\Mail\Header\Exception;

use Zend\Mail\Exception;

class RuntimeException extends Exception\RuntimeException implements ExceptionInterface
{
}
