<?php

namespace Zend\Mail\Storage\Exception;

use Zend\Mail\Exception;

/**
 * Exception for Laminas\Mail component.
 */
class RuntimeException extends Exception\RuntimeException implements ExceptionInterface
{
}
