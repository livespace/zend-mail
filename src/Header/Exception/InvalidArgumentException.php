<?php

namespace Zend\Mail\Header\Exception;

use Zend\Mail\Exception;

class InvalidArgumentException extends Exception\InvalidArgumentException implements ExceptionInterface
{
}
