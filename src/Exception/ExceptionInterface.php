<?php

namespace Zend\Mail\Exception;

use Throwable;

interface ExceptionInterface extends Throwable
{
}
