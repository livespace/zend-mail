<?php

namespace Zend\Mail\Exception;

/**
 * Exception for Laminas\Mail component.
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
