<?php

namespace Zend\Mail\Protocol\Exception;

use Zend\Mail\Exception\ExceptionInterface as MailException;

interface ExceptionInterface extends MailException
{
}
