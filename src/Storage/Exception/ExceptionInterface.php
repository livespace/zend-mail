<?php

namespace Zend\Mail\Storage\Exception;

use Zend\Mail\Exception\ExceptionInterface as MailException;

interface ExceptionInterface extends MailException
{
}
