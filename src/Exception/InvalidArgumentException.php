<?php

namespace Zend\Mail\Exception;

/**
 * Exception for Laminas\Mail component.
 */
class InvalidArgumentException extends \InvalidArgumentException implements
    ExceptionInterface
{
}
