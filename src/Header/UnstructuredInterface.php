<?php

namespace Zend\Mail\Header;

/**
 * Marker interface for unstructured headers.
 */
interface UnstructuredInterface extends HeaderInterface
{
}
