<?php

namespace Zend\Mail\Exception;

/**
 * Exception for Laminas\Mail component.
 */
class BadMethodCallException extends \BadMethodCallException implements
    ExceptionInterface
{
}
