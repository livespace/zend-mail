<?php

namespace Zend\Mail;

class Module
{
    /**
     * Retrieve zend-mail package configuration for zend-mvc context.
     *
     * @return array
     */
    public function getConfig()
    {
        $provider = new ConfigProvider();
        return [
            'service_manager' => $provider->getDependencyConfig(),
        ];
    }
}
