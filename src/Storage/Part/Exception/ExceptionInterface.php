<?php

namespace Zend\Mail\Storage\Part\Exception;

use Zend\Mail\Storage\Exception\ExceptionInterface as StorageException;

interface ExceptionInterface extends StorageException
{
}
