<?php

namespace Zend\Mail\Exception;

/**
 * Exception for Laminas\Mail component.
 */
class OutOfBoundsException extends \OutOfBoundsException implements ExceptionInterface
{
}
