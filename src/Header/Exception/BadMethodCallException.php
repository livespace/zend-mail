<?php

namespace Zend\Mail\Header\Exception;

use Zend\Mail\Exception;

class BadMethodCallException extends Exception\BadMethodCallException implements ExceptionInterface
{
}
