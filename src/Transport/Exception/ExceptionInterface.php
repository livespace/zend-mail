<?php

namespace Zend\Mail\Transport\Exception;

use Zend\Mail\Exception\ExceptionInterface as MailException;

interface ExceptionInterface extends MailException
{
}
