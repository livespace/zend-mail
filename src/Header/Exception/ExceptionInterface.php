<?php

namespace Zend\Mail\Header\Exception;

use Zend\Mail\Exception\ExceptionInterface as MailException;

interface ExceptionInterface extends MailException
{
}
