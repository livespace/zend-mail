<?php

namespace Zend\Mail\Storage\Part\Exception;

use Zend\Mail\Storage\Exception;

/**
 * Exception for Laminas\Mail component.
 */
class InvalidArgumentException extends Exception\InvalidArgumentException implements ExceptionInterface
{
}
