<?php

namespace Zend\Mail\Protocol\Exception;

use Zend\Mail\Exception;

/**
 * Exception for Laminas\Mail component.
 */
class InvalidArgumentException extends Exception\InvalidArgumentException implements ExceptionInterface
{
}
