<?php

namespace Zend\Mail\Transport\Exception;

use Zend\Mail\Exception;

/**
 * Exception for Zend\Mail\Transport component.
 */
class DomainException extends Exception\DomainException implements ExceptionInterface
{
}
